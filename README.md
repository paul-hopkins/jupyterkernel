# JupyterKernel

## Collection of docker/singularity images based on ligo reference OSes 

    suitable for use a jupyter kernels but also as condor payloads.

thanks to automation at git.ligo.org these images are automatically 
published over cvmfs in the form of singularity images. 


- jupyterkernel:strech based on ligo/software:stretch 
- jupyterkernel:lite smaller image based on ligo/base:stretch
    primarily intended for python 
- jupyterkernel:sl7  based on ligo/software:sl7


## How to use the image:

- Dockerfile:
  
   ``` 
    FROM containers.ligo.org/philippe.grassia/jupyterkernel:stretch
  ```  
  
- Singularity over cvmfs 
    
   ```bash
   singularity shell -w /cvmfs/ligo-containers.opensciencegrid.org/philippe.grassia/jupyterkernel/stretch
   ```
   
- Condor job: (untested)
  
  ```
   SingularityImage = "/cvmfs/ligo-containers.opensciencegrid.org/philippe.grassia/jupyterkernel:stretch"
  ```
  
- as jupyter kernel:
   - python2 kernel.json:
     ```    
     {
         "language": "python",
         "argv": ["/usr/bin/singularity",
           "exec",
           "-w",
           "/cvmfs/ligo-containers.opensciencegrid.org/philippe.grassia/jupyterkernel/stretch",
           "/usr/bin/python2",
           "-m",
           "ipykernel",
           "-f",
           "{connection_file}"
         ],
         "display_name": " LIGO RefOS python2"
        }
     ```
   - python3 kernel.json:
     ```
     {
         "language": "python",
         "argv": ["/usr/bin/singularity",
           "exec",
           "-w",
           "/cvmfs/ligo-containers.opensciencegrid.org/philippe.grassia/jupyterkernel/stretch",
           "/usr/bin/python3",
           "-m",
           "ipykernel",
           "-f",
           "{connection_file}"
         ],
         "display_name": "LIGO RefOS python3"
        }
     ```
